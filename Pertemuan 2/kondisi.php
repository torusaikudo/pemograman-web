<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kondisi PHP</title>
</head>
<body>
    <h1>Berlatih PHP Function</h1>
    
    <?php
        echo "<h3>Soal No 1 Greatings</h3>";

        // Contoh Function Greating
        greetings("Feby");
        greetings("Binan");
        // coba function disini
        function greetings($nama){
            echo " Hallo ".$nama.", Selamat datang di Web InI<br>";
        }

        echo "<h3>Soal No 2</h3>";

        // Code Function reverse
        reverseString("feby");
        reverseString("Pemograman Web");
        echo "<br>";

        function reverseString($kata2){
            $balik=reverse($kata2);
            echo $balik."<br>";
            }

        function reverse($kata1){
            $panjangstring=strlen($kata1);
            $tampung="";
            for($t=$panjangstring-1;$t>=0;$t--){
                $tampung.=$kata1[$t];
            }
            return $tampung;
        }
    ?>
</body>
</html>